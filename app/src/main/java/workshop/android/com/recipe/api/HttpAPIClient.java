package workshop.android.com.recipe.api;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by anwarif on 08/10/2016.
 */

public class HttpAPIClient {
    private static HttpAPIClient mSharedInstance;

    private RequestQueue mRequestQueue;

    public static synchronized HttpAPIClient getSharedInstance(Context context){
        if (mSharedInstance == null){
            mSharedInstance = new HttpAPIClient();
            mSharedInstance.mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return mSharedInstance;
    }

    private HttpAPIClient(){
    }

    public interface OnHtmlSourceLoadedListener {
        void htmlSourceLoaded(String htmlString, VolleyError error);
    }

    public void getHtmlSource(String url, final OnHtmlSourceLoadedListener handler){
        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handler.htmlSourceLoaded(response, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handler.htmlSourceLoaded(null, error);
            }
        });

        mRequestQueue.add(request);
    }
}
