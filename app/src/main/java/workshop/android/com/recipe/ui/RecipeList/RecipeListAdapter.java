package workshop.android.com.recipe.ui.RecipeList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import workshop.android.com.recipe.R;
import workshop.android.com.recipe.model.Recipe;

/**
 * Created by Syed on 01/10/2016.
 */

public class RecipeListAdapter extends RecyclerView.Adapter {
    private List<Recipe> mRecipeList = new ArrayList<>();

    public List<Recipe> getRecipeList() {
        return mRecipeList;
    }

    public void setRecipeList(List<Recipe> recipeList) {
        mRecipeList.clear();
        mRecipeList.addAll(recipeList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        NumberViewHolder viewHolder = new NumberViewHolder(itemView);

        return viewHolder;*/

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.cell_recipe, parent, false);
        return new RecipeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        /*TextView titleTextView = (TextView) holder.itemView;
        titleTextView.setText("Item " + position);*/

        RecipeViewHolder recipeViewHolder = (RecipeViewHolder) holder;
        Recipe recipe = mRecipeList.get(position);
    }

    @Override
    public int getItemCount() {
        return mRecipeList.size();
    }



    /*class NumberViewHolder extends RecyclerView.ViewHolder{
        public NumberViewHolder(View itemView){
            super(itemView);
        }
    }*/
}
