package workshop.android.com.recipe.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import workshop.android.com.recipe.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeaturedFragment extends Fragment {
    public static final String TAG = "FeaturedFragment";
    private Button mRecipeButton;

    public FeaturedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_featured, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //mRecipeButton = (Button) view.findViewById(R.id.activity_main_vg_container_top);
        mRecipeButton = (Button) view.findViewById(R.id.fragment_featured_btn_view_recipe);
        mRecipeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_main_vg_container, new RecipeDetailFragment())
                        .addToBackStack(new RecipeDetailFragment().TAG)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit();
            }
        });

    }
}
