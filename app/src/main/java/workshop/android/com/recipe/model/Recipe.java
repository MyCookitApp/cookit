package workshop.android.com.recipe.model;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Syed on 04/10/2016.
 */

public class Recipe {
    private String mTitle;
    private String mDescription;
    private String mImageUrl;
    private List<Ingredient> Ingredients;
    private List<CookingStep> CookingSteps;

    //region Constructors
    public Recipe(){
        mTitle = "Something's is cooking";
        mDescription = "Someone's kitchen is on fire tonight";
    }

    public Recipe(String title, String description, String imageURL, List<Ingredient> ingredients, List<CookingStep> cookingSteps) {
        mTitle = title;
        mDescription = description;
        mImageUrl = imageURL;
        Ingredients = ingredients;
        CookingSteps = cookingSteps;
    }

    public Recipe(@NonNull JSONObject jsonObject) throws Exception {
        mTitle = jsonObject.getString("title");
        mDescription = jsonObject.getString("description");
    }

    public JSONObject toJsonObject(){
        JSONObject jsonProduct = new JSONObject();

        try{
            jsonProduct.put("title", this.getTitle());
            jsonProduct.put("description", this.getDescription());
        } catch (JSONException jexception) {
            jexception.printStackTrace();
        }

        return jsonProduct;
    }
    //endregion Constructors

    //region Parcelable

    //endregion Parcelable

    //region Setters & Getters
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImageURL() {
        return mImageUrl;
    }

    public void setImageURL(String imageURL) {
        mImageUrl = imageURL;
    }

    public List<Ingredient> getIngredients() {
        return Ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        Ingredients = ingredients;
    }

    public List<CookingStep> getCookingSteps() {
        return CookingSteps;
    }

    public void setCookingSteps(List<CookingStep> cookingSteps) {
        CookingSteps = cookingSteps;
    }
    //endregion Setters & Getters
}
