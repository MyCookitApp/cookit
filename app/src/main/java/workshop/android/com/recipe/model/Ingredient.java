package workshop.android.com.recipe.model;

/**
 * Created by Syed on 04/10/2016.
 */

public class Ingredient {
    private int mSequence;
    private int mValue;
    private String mUnit;
    private String mItem;

    public Ingredient(int sequence, int value, String unit, String item) {
        mSequence = sequence;
        mValue = value;
        mUnit = unit;
        mItem = item;
    }

    public int getSequence() {
        return mSequence;
    }

    public void setSequence(int sequence) {
        mSequence = sequence;
    }

    public int getValue() {
        return mValue;
    }

    public void setValue(int value) {
        mValue = value;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public String getItem() {
        return mItem;
    }

    public void setItem(String item) {
        mItem = item;
    }
}
