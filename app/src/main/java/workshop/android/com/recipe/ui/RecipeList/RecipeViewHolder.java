package workshop.android.com.recipe.ui.RecipeList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import workshop.android.com.recipe.R;
import workshop.android.com.recipe.model.Recipe;

/**
 * Created by Syed on 6/10/2016.
 */

public class RecipeViewHolder extends RecyclerView.ViewHolder {
    private ImageView mRecipeImageView;
    private TextView mTitleTextView;
    private TextView mDescriptionTextView;
    //private TextView mLikeTextView;
    //private TextView mRecommendedTextView;

    private Recipe mRecipe;

    public RecipeViewHolder(View itemView) {
        super(itemView);

        mRecipeImageView = (ImageView) itemView.findViewById(R.id.cell_recipe_image);
        mTitleTextView = (TextView) itemView.findViewById(R.id.cell_recipe_title);
        mDescriptionTextView = (TextView) itemView.findViewById(R.id.cell_recipe_description);
    }

    public Recipe getmRecipe() {
        return mRecipe;
    }

    public void setmRecipe(Recipe mRecipe) {
        this.mRecipe = mRecipe;

        updateDisplay();
    }

    private void updateDisplay(){
        //Bitmap recipeImage = (Bitmap) Bitmap.createBitmap()
        //mRecipeImageView.setImageURI(mRecipe.getImageURL());
        mTitleTextView.setText(mRecipe.getTitle());
        mDescriptionTextView.setText(mRecipe.getDescription());

    }
}
