package workshop.android.com.recipe.controller;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import workshop.android.com.recipe.R;
import workshop.android.com.recipe.model.CookingStep;
import workshop.android.com.recipe.model.Ingredient;
import workshop.android.com.recipe.model.Recipe;

/**
 * Created by Syed on 07/10/2016.
 */

public class RecipeLoader {
    private static final String TAG = "RecipeLoader";

    public static List<Recipe> loadSamples(Context context){
        ArrayList<Recipe> sampleRecipes = new ArrayList<>();
        ArrayList<Ingredient> sampleIngredients = new ArrayList<>();
        ArrayList<CookingStep> sampleSteps = new ArrayList<>();

        InputStream inputStream = context.getResources().openRawResource(R.raw.recipes);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try{
            String line;
            while ((line = reader.readLine()) != null){
                String [] tokens = line.split(";");

                String imageUrl = tokens[0];
                String title = tokens[1];
                String description = tokens[2];

                Recipe recipe = new Recipe(title,description,imageUrl, sampleIngredients, sampleSteps);
                sampleRecipes.add(recipe);
            }
        } catch (Exception exception){
            Log.e(TAG, "Load exception: " + exception.toString());
        }

        return sampleRecipes;
    }

    public static Recipe loadJSONSample(Context context){
        InputStream inputStream = context.getResources().openRawResource(R.raw.recipe);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        JSONObject jsonObject = new JSONObject();
        Recipe recipe = new Recipe();

        try{
            while ((line = reader.readLine()) != null) {
                jsonObject = new JSONObject(line);

                recipe = new Recipe(jsonObject);
            }
        } catch (Exception exception) {
            Log.e(TAG, "load JSON exception: " + exception.toString());
        }

        return recipe;
    }
}
