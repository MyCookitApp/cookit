package workshop.android.com.recipe.model;

import android.media.Image;

import java.util.List;

/**
 * Created by Syed on 04/10/2016.
 */

public class Chef {
    private String mEmail;
    private String mPassword;
    private String mChefName;
    private Image mProfilePhoto;
    private List<Recipe> mChefRecipes;
    private List<Favourite> mChefFavourites;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getChefName() {
        return mChefName;
    }

    public void setChefName(String chefName) {
        mChefName = chefName;
    }

    public Image getProfilePhoto() {
        return mProfilePhoto;
    }

    public void setProfilePhoto(Image profilePhoto) {
        mProfilePhoto = profilePhoto;
    }

    public List<Recipe> getChefRecipes() {
        return mChefRecipes;
    }

    public void setChefRecipes(List<Recipe> chefRecipes) {
        mChefRecipes = chefRecipes;
    }

    public List<Favourite> getChefFavourites() {
        return mChefFavourites;
    }

    public void setChefFavourites(List<Favourite> chefFavourites) {
        mChefFavourites = chefFavourites;
    }
}
