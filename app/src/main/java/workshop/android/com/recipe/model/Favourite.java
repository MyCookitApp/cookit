package workshop.android.com.recipe.model;

/**
 * Created by Syed on 04/10/2016.
 */

public class Favourite {
    private Recipe mFavRecipes;

    public Recipe getRecipes() {
        return mFavRecipes;
    }

    public void setRecipes(Recipe recipes) {
        mFavRecipes = recipes;
    }
}
