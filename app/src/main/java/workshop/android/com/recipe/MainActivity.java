package workshop.android.com.recipe;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewGroupCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;

import workshop.android.com.recipe.api.HttpAPIClient;
import workshop.android.com.recipe.ui.FeaturedFragment;
import workshop.android.com.recipe.ui.RecipeList.RecipeListFragment;
import workshop.android.com.recipe.ui.UnderConstructionFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout mDrawerLayout;
    private ViewGroup mViewGroup;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;

    private SharedPreferences mSharedPreferences;
    private int mLaunchCount;
    private TextView mSourceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawer_layout);
        mViewGroup = (ViewGroup) findViewById(R.id.activity_main_vg_container);
        mNavigationView = (NavigationView) findViewById(R.id.activity_main_navigation_view);
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);

        // Set toolbar as appbar
        setSupportActionBar(mToolbar);

        // Create ActionBarDrawerToggle
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        // Populatie drawer menu
        mNavigationView.inflateMenu(R.menu.navigation_drawer_menu);
        mNavigationView.setNavigationItemSelectedListener(this);

        //Retrieve SharedPreference items
//        mSharedPreferences = getSharedPreferences("workshop.android.com.recipe.PREFERENCE_FILE_KEY", MODE_PRIVATE);
//        mLaunchCount = mSharedPreferences.getInt("LAUNCH_COUNT", 0);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_main_vg_container, new RecipeListFragment())
                    //.add(R.id.activity_main_vg_container_top, new FeaturedFragment())
                    //.add(R.id.activity_main_vg_container_middle, new RelatedFragment())
                    //.add(R.id.activity_main_vg_container_bottom, new RecipeListFragment())
                    .commit();
        }

        //mSourceTextView = (TextView) findViewById(R.id.activity_main_tv);
    }


    //Navigation menu listener
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        changeFragment(id);

        mDrawerLayout.closeDrawers();

        return false;
    }

    private void changeFragment(int id){
        Fragment displayFragment = null;

        switch (id) {
            case R.id.nav_add_new_recipe:
                displayFragment = new RecipeListFragment();
                break;
            case R.id.nav_my_cookbook:
                displayFragment = new RecipeListFragment();
                break;
            case R.id.nav_favourites:
                displayFragment = new RecipeListFragment();
                break;
            case R.id.nav_what_to_cook:
                displayFragment = new UnderConstructionFragment();
                break;
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, displayFragment)
                .commit();
    }

    //Getting the HTMLSource from other sites
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        String url = "https://developer.android.com/index.html";
//        HttpAPIClient apiClient = HttpAPIClient.getSharedInstance(getApplicationContext());
//
//        apiClient.getHtmlSource(url, new HttpAPIClient.OnHtmlSourceLoadedListener() {
//            @Override
//            public void htmlSourceLoaded(String htmlString, VolleyError error) {
//                if (htmlString != null){
//                    mSourceTextView.setText(htmlString);
//                } else {
//                    mSourceTextView.setText(error.toString());
//                }
//            }
//        });
//    }
}
