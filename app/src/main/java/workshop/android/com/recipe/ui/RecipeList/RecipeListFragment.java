package workshop.android.com.recipe.ui.RecipeList;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import workshop.android.com.recipe.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeListFragment extends Fragment {
    private RecyclerView mRecyclerView;

    public RecipeListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_recipe_list, container, false);
        View rootView = inflater.inflate(R.layout.fragment_recipe_list, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_recipe_rv_list);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);

        RecipeListAdapter recipeListAdapter = new RecipeListAdapter();
        //recipeListAdapter.setRecipeList();
        mRecyclerView.setAdapter(recipeListAdapter);
    }
}
