package workshop.android.com.recipe.model;

import android.media.Image;

/**
 * Created by Syed on 04/10/2016.
 */

public class CookingStep {
    private int mSequence;
    private String mDescription;
    private String mSeqImageUrl;

    public CookingStep(int sequence, String description, String seqImageUrl) {
        mSequence = sequence;
        mDescription = description;
        mSeqImageUrl = seqImageUrl;
    }

    public int getSequence() {
        return mSequence;
    }

    public void setSequence(int sequence) {
        mSequence = sequence;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getSeqImageUrl() {
        return mSeqImageUrl;
    }

    public void setSeqImageUrl(String seqImageUrl) {
        mSeqImageUrl = seqImageUrl;
    }
}
